
from django.contrib import admin
from django.urls import path, include
from GestionApp import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('gestion/',include('GestionApp.urls') ),
    path('accounts/', include('registration.backends.default.urls')),

]
