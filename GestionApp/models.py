from django.db import models
from datetime import datetime 
from django.dispatch import receiver
from django.urls import reverse
from django.db.models.signals import post_delete

# Create your models here.

class Empleado(models.Model):
    cedula = models.CharField(max_length=100)
    genero = models.CharField(max_length=100)
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    telefono = models.CharField(max_length=100)
    estado = models.BooleanField(default=False)
    edad = models.IntegerField()
    foto = models.ImageField(upload_to='photos/')

    def __str__(self):
        return self.nombres

    def get_absolute_url(self):
        return reverse('empleado-list')

@receiver(post_delete, sender=Empleado)
def foto_delete(sender, instance, **kwargs):
    """ Borra los ficheros de las fotos que se eliminan. """
    instance.foto.delete(False)

class Gestion(models.Model):
     empleado = models.ForeignKey('Empleado', on_delete=models.PROTECT)
     horaEntrada = models.DateTimeField(default=datetime.now,blank=True)
     horaSalida = models.DateTimeField(default=datetime.now,blank=True)

     def get_absolute_url(self):
        return reverse('empleado-list')
     
     def __str__(self):
        return self.empleado.nombres
    
    
