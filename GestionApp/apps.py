from django.apps import AppConfig


class GestionappConfig(AppConfig):
    name = 'GestionApp'
