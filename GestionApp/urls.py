
from django.contrib import admin
from django.urls import path, include, re_path
from GestionApp import views

urlpatterns = [
   path('admin/', admin.site.urls),
   path('',views.first_view, name='first_view' ),
   path('empleado/crear/',views.EmpleadoCreate.as_view(), name='crear-empleado' ),
   path('empleado/listar/',views.EmpleadoListView.as_view(), name='empleado-list' ),
   path('empleado/<int:pk>/eliminar/', views.EmpleadoDelete.as_view(), name='empleado-eliminar'),
   path('empleado/<int:pk>/modificar/', views.EmpleadoUpdate.as_view(), name='empleado-modificar'),
   path('empleado/<int:pk>/detalle/', views.EmpleadoDetailView.as_view(), name='empleado-detalle'),
   path('empleado/<int:pk>/detalle2/', views.empleado_detail2, name='empleado-detalle2'),
   path('gestion/listar/',views.GestionListView.as_view(), name='gestion-list' ),
   path('gestion/crear/',views.GestionCreate.as_view(), name='crear-gestion' ),
   path('accounts/', include('registration.backends.default.urls')),
   path('empleado/<str:pCedula>/buscar/',views.buscar_empleado, name='buscar-empleado' ),

]