from django.shortcuts import render

from django.http import HttpResponse
from django.dispatch import receiver
from django.db.models.signals import post_delete
from GestionApp.models import Empleado, Gestion
from django.views.generic import ListView, DetailView
from django.urls import reverse_lazy
from django.shortcuts import render
from datetime import datetime, timedelta

from django.views.generic.edit import UpdateView, CreateView, DeleteView
# Create your views here.
def first_view(request):
    return render(request, 'base.html') 

class EmpleadoListView(ListView):
    model = Empleado

class EmpleadoDetailView(DetailView):
    model = Empleado

    
class EmpleadoUpdate(UpdateView):
	model = Empleado
	fields = '__all__'

class EmpleadoCreate(CreateView):
	model = Empleado
	fields = '__all__'

class GestionCreate(CreateView):
    model = Gestion
    fields='__all__'

class EmpleadoDelete(DeleteView):
    model = Empleado
    success_url = reverse_lazy('empleado-list')

class GestionListView(ListView):
    model = Gestion

def empleado_detail2(request, pk):
    empleado = Empleado.objects.get(id=pk)
    context = {'object': empleado}
    return render(request, 'GestionApp/empleado_detail2.html', context)

def buscar_empleado(request, pCedula):
   empleado1=Empleado.objects.filter(cedula=pCedula).first()
   context = {'object': empleado1}
   gestion = Gestion(empleado=empleado1,horaEntrada=datetime.now,horaSalida=datetime.now)
   gestion.save()
   return render(request, 'GestionApp/gestion_form.html', context)







